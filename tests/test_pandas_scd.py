from pandas.testing import assert_frame_equal
import unittest
import pandas as pd
from freezegun import freeze_time
from datetime import datetime
import sys
import pathlib
sys.path.append(str(pathlib.Path.cwd() / 'src'))
from scd2 import SCD2
import copy




class TestSCD(unittest.TestCase):

    # @freeze_time("2012-01-14 03:21:34")
    # def test_initial_run_on_empty_df(self):
    #     src = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul']})
    #     tgt = pd.DataFrame(columns = ["first_name", "last_name", "start_ts", "end_ts", "is_active"])
    #     tracked_columns = ["first_name", "last_name"]
    #     scd_df = scd(src, tgt, tracked_columns)
    #     expected = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul'], "start_ts": [datetime(2012, 1, 14, 3, 21, 34)], "end_ts": [None], "is_active": [True]})
    #     assert_frame_equal(expected, scd_df, check_dtype=False)


    def test_unchanged_active_keys(self):
        src = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul']})
        tgt = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul'], 'start_ts': [datetime(2012, 1, 14, 3, 21, 34)], 'end_ts': [None], 'is_active': [True]})
        tracked_columns = ["first_name", "last_name"]
        scd_df = SCD2().pandas_scd2(src, tgt, tracked_columns)
        expected = copy.deepcopy(tgt)
        assert_frame_equal(expected, scd_df, check_dtype=False)


    @freeze_time("2020-01-01 00:00:00")
    def test_unchanged_inactive_keys(self):
        src = pd.DataFrame(columns = ["first_name", "last_name"])
        tgt = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul'], 'start_ts': [datetime(2012, 1, 14, 3, 21, 34)], 'end_ts': [datetime(2012, 1, 15, 3, 21, 34)], 'is_active': [False]})
        tracked_columns = ["first_name", "last_name"]
        scd_df = SCD2().pandas_scd2(src, tgt, tracked_columns)
        expected = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul'], 'start_ts': [datetime(2012, 1, 14, 3, 21, 34)], 'end_ts': [datetime(2012, 1, 15, 3, 21, 34)], 'is_active': [False]})
        assert_frame_equal(expected, scd_df, check_dtype=False)


    @freeze_time("2012-01-14 03:21:34")
    def test_new_keys(self):
        src = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul']})
        tgt = pd.DataFrame(columns = ["first_name", "last_name", "start_ts", "end_ts", "is_active"])
        tracked_columns = ["first_name", "last_name"]
        scd_df = SCD2().pandas_scd2(src, tgt, tracked_columns)
        expected = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul'], "start_ts": [datetime(2012, 1, 14, 3, 21, 34)], "end_ts": [None], "is_active": [True]})
        assert_frame_equal(expected, scd_df, check_dtype=False)


    @freeze_time("2018-01-01 00:00:00")
    def test_ended_keys(self):
        src = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Rock']})
        tgt = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul'], "start_ts": [datetime(2012, 1, 14, 3, 21, 34)], "end_ts": [None], "is_active": [True]})
        tracked_columns = ["first_name", "last_name"]
        scd_df = SCD2().pandas_scd2(src, tgt, tracked_columns)
        expected = pd.DataFrame.from_dict({'first_name': ["Chris", "Chris"], 'last_name': ['Paul', "Rock"], "start_ts": [datetime(2012, 1, 14, 3, 21, 34), datetime(2018, 1, 1, 0, 0, 0)], "end_ts": [datetime(2018, 1, 1, 0, 0, 0), None], "is_active": [False, True]})
        assert_frame_equal(expected, scd_df, check_dtype=False)


    @freeze_time("2018-01-01 00:00:00")
    def test_untracked_columns_unchanged_active_key(self):
        src = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul'], 'team': ['Suns']})
        tgt = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul'], 'team': ["Clippers"], "start_ts": [datetime(2012, 1, 14, 3, 21, 34)], "end_ts": [None], "is_active": [True]})
        tracked_columns = ["first_name", "last_name"]
        scd_df = SCD2().pandas_scd2(src, tgt, tracked_columns)
        expected = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul'], 'team': ["Clippers"], "start_ts": [datetime(2012, 1, 14, 3, 21, 34)], "end_ts": [None], "is_active": [True]})
        assert_frame_equal(expected, scd_df, check_dtype=False)


    @freeze_time("2018-01-01 00:00:00")
    def test_tracked_columns(self):
        src = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul'], 'team': ['Suns']})
        tgt = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul'], 'team': ["Clippers"], "start_ts": [datetime(2012, 1, 14, 3, 21, 34)], "end_ts": [None], "is_active": [True]})
        
        #explicit tracked columns
        tracked_columns = ["first_name", "last_name"]
        scd_df = SCD2().pandas_scd2(src, tgt, tracked_columns)
        expected = pd.DataFrame.from_dict({'first_name': ["Chris"], 'last_name': ['Paul'], 'team': ["Clippers"], "start_ts": [datetime(2012, 1, 14, 3, 21, 34)], "end_ts": [None], "is_active": [True]})
        assert_frame_equal(expected, scd_df, check_dtype=False)
        
        #none tracked columns
        scd_df = SCD2().pandas_scd2(src, tgt)
        expected = pd.DataFrame.from_dict({'first_name': ["Chris", "Chris"], 'last_name': ['Paul', 'Paul'], 'team': ["Clippers", "Suns"], "start_ts": [datetime(2012, 1, 14, 3, 21, 34), datetime(2018, 1, 1, 0, 0, 0)], "end_ts": [datetime(2018, 1, 1, 0, 0, 0), None], "is_active": [False, True]})
        assert_frame_equal(expected, scd_df, check_dtype=False)


if __name__ == '__main__':
    unittest.main()
