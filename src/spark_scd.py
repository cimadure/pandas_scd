from pyspark.sql.functions import current_timestamp, lit, col, sha2, concat_ws, coalesce
import pyspark.sql as spark


#TODO cols_to_track = None -> use all columns to track
#TODO add columns that aren't tracked to the final table
def spark_scd(src: spark.DataFrame, tgt: spark.DataFrame, cols_to_track: list) -> spark.DataFrame:
    """
    src: pandas dataframe with the source of the SCD
    tgt: pandas dataframe with the target of the SCD (target can be empty)
    cols_to_track: list of columns to track changes
    the return dataframe contain the entire target table with the new changes, ready for insert overwrite of the current target table
    """
   
    if not isinstance(cols_to_track, list):
        raise TypeError('cols_to_track must be of type list')

    src_cols = [c + '_src' for c in cols_to_track]

    tgt = tgt.withColumn("hash", sha2(concat_ws("", *cols_to_track), 256))
    src = src.withColumn("hash", sha2(concat_ws("", *cols_to_track), 256))

    src = src.select(*(col(x).alias(x + '_src') for x in src.columns))

    # rows that are in both target and source, and active
    unchanged_keys = src.join(tgt, [src.hash_src == tgt.hash, tgt.is_current == True], 'inner')\
        .select(
            "start_ts",
            "end_ts",
            "is_current",
            *cols_to_track
        )

    # rows that are in the target but no longer in the source
    changed_keys = tgt.join(src, src.hash_src == tgt.hash, 'left_anti')\
        .select(
         "start_ts",
         coalesce("end_ts", current_timestamp()).alias("end_ts"),
            *cols_to_track
            )\
        .withColumn("is_current", lit(False))
   

    # rows that are in the source but not in the target
    keys_to_insert = src.join(tgt, src.hash_src == tgt.hash, 'left_anti')\
        .select(*src_cols)\
        .withColumn("start_ts", lit(current_timestamp()))\
        .withColumn("end_ts", lit(None))\
        .withColumn("is_current", lit(True))

    for c in keys_to_insert.columns:
        keys_to_insert = keys_to_insert.withColumnRenamed(c, c.replace('_src', ''))

    return unchanged_keys.unionByName(changed_keys).unionByName(keys_to_insert)